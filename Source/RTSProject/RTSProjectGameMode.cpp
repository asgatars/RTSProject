// Copyright Epic Games, Inc. All Rights Reserved.

#include "RTSProjectGameMode.h"
#include "RTSProjectPlayerController.h"
#include "RTSProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARTSProjectGameMode::ARTSProjectGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ARTSProjectPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}